//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>
#include <cmath>


    class MidiMessage
    {
    public:
        MidiMessage()
        {
            number = 60;
            velocity = 127.0;
            channelNum = 1;
        }
        
        ~MidiMessage()
        {
            
        }
        void setNoteNumber(int value)
        {
            if(value < 128 && value > -1)
            {
            number = value;
            }
        }
        int getNoteNumber() const
        {
            return number;
        }
        float getMidiNoteInHertz()
        {
            return 440* pow(2, (number-69) / 12.0);
        }
        void setVelocity(int value)
        {
            velocity = value;
        }
        int getVelocity() const
        {
            return velocity;
        }
        float getFloatVelocity()
        {
            velocity /= 127.0;
            return velocity;
        }
        void setChannelNumber(int value)
        {
            channelNum = value;
        }
        int getChannelNumber() const
        {
            return channelNum;
        }
    private:
        int number;
        int velocity;
        int channelNum;
    };

int main()
{
    MidiMessage note;
    
    int noteNumber;
    
    std::cout << "Note Number = " << note.getNoteNumber() << "\n Frequency = " << note.getMidiNoteInHertz() << "\n Velocity = " << note.getFloatVelocity() << "\n Channel Number = " << note.getChannelNumber() << std::endl;
    
    std::cin >> noteNumber;
    
    note.setNoteNumber(noteNumber);
    
    std::cout << "Note Number = " << note.getNoteNumber() << "\n Frequency = " << note.getMidiNoteInHertz();
    
    return 0;
}
